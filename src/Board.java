import java.util.Scanner;

public class Board {
	private static String[][] table = new String[3][3];
	private static Player x;
	private static Player o;
	private static Player winner;
	private static Player currentPlayer;
	private static int turnCount;

	 	
	 	
	 	public Board(Player x,Player o) {
	 		this.x=x;
	 		this.o=o;
	 		currentPlayer=x;
	 		turnCount = 0;
	 		winner=null;
	 		showTable();
	 	}
	 	
		public static void input() {
			Scanner ms = new Scanner(System.in);
			while (winner == null) {
				showTurn();
				int row = ms.nextInt();
				int col = ms.nextInt();
				if (OutofBoard(row, col) == true) {
					System.out.println("You've inputed place, which is out of the board.\nPlease try again.");
				} else {
					if (isFull(row, col)) {
						System.err.println("Please try again.\n");

					} else {
						table[row - 1][col - 1] = getCurrentPlayer().getName();
						printBoard();
						if (Winner() == false) {
							if (currentPlayer == x) {
								winner = x;
								System.out.println("The winner is "
										+ getCurrentPlayer().getName());
							} else {
								winner = o;
								System.out.println("The winner is "
										+ getCurrentPlayer().getName());
							}
						} else {
							changePlayer();
							turnCount++;
						}
					}

				}
				if (turnCount > 8) {
					System.out.println("Draw!!");
					break;
				}

			}
			if (getWinner() == x) {
				x.setWin();
				o.setLose();
			} else if (getWinner() == o) {
				o.setWin();
				x.setLose();
			} else {
				o.setDraw();
				x.setDraw();
			}
			if (getWinner() == x) {
				x.setWin();
				o.setLose();
			} else if (getWinner() == o) {
				o.setWin();
				x.setLose();
			} else {
				o.setDraw();
				x.setDraw();
			}

		}

		
		public static boolean OutofBoard (int row, int col) {
			if (row > 3 || col > 3) {
				System.err.println("There is no row or column");
				return true;

			}
			return false;
		}

		public static Player getCurrentPlayer() {
			return currentPlayer;
		}
		
		public static void changePlayer() {
			if (currentPlayer == x) {
				currentPlayer = o;
			} else {
				currentPlayer = x;
			}
		}

		
		public static Player getWinner() {
			return winner;
		}
	 	
		public void showTable() {
			for (int i = 0; i < table.length; i++) {
				for (int j = 0; j < table[i].length; j++) {
					table[i][j] = "-";
				}
			}
		}
	 	
		public static void printBoard() {
			System.out.println("   " + "1  " + " 2  " + " 3  ");

			for (int i = 0; i < table.length; i++) {
				System.out.print(i + 1 + "  ");
				for (int j = 0; j < table[i].length; j++) {
					System.out.print(table[i][j] + "   ");
				}
				System.out.println();
			}
		}

	 	
	 	public static void showTurn() {
			System.out.println(currentPlayer.getName() + " turn..");
			System.out.print("Please input row and column: ");

		}


		public static boolean isFull(int row, int col) {
			if (table[row - 1][col - 1] == "X" || table[row - 1][col - 1] == "O") {
				return true;
			}
			return false;
		}

		
		public Player getDraw() {
			return winner;
		}
		
		public static boolean Winner() {
			if (table[0][0] != "-" && table[0][0] == table[0][1]
					&& table[0][0] == table[0][2])
				return false;
			if (table[1][0] != "-" && table[1][0] == table[1][1]
					&& table[1][0] == table[1][2])
				return false;
			if (table[2][0] != "-" && table[2][0] == table[2][1]
					&& table[2][0] == table[2][2])
				return false;
			if (table[0][0] != "-" && table[0][0] == table[1][0]
					&& table[0][0] == table[2][0])
				return false;
			if (table[0][1] != "-" && table[0][1] == table[1][1]
					&& table[0][1] == table[2][1])
				return false;
			if (table[0][2] != "-" && table[0][2] == table[1][2]
					&& table[0][2] == table[2][2])
				return false;
			if (table[0][0] != "-" && table[0][0] == table[1][1]
					&& table[0][0] == table[2][2])
				return false;
			if (table[0][2] != "-" && table[0][2] == table[1][1]
					&& table[0][2] == table[2][0])
				return false;
			return true;
		}


	
		
}
