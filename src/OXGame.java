import java.util.Scanner;

public class OXGame {
	private static Board board;
	private static Player winner;
	private static Player currentPlayer;
	private static Player o;
	private static Player x;

	public OXGame() {
		o = new Player("O");
		x = new Player("X");
		board = new Board(x, o);

	}

	public void Play() {
		showWelcome();
		board.printBoard();
		board.input();
		showStat();
		showBye();
		playagain();

		
	}

	public void showWelcome() {
		System.out.println("Welcome to OX Game!!!");
	}

	private static void showTurn() {
		System.out.println(board.getCurrentPlayer().getName() + " turn...");
	}

	private void showBye() {
		System.out.println("GoodBye.");
	}

	private void showStat() {
		System.out.println("Stat: ");
		System.out.println("   Player   " + "Win  " + " Lose  " + " Draw  ");
		System.out.println("     X       " + x.getWin() + "      "+ x.getLose() + "     " + x.getDraw() + "  ");
		System.out.println("     O       " + o.getWin() + "      "+ o.getLose() + "     " + o.getDraw() + "  ");

	}
	
	public static boolean OutOfBoard(int row, int col) {
		if (row > 3 || col > 3) {
			System.err.println("There is no row or column");
			return true;

		}
		return false;
	}
	
	private void playagain() {
		Scanner ms = new Scanner(System.in);
		System.out.println("Do you want to play again? <yes/no>:");
		String answer = ms.next();
		if (answer.equalsIgnoreCase("yes")) {
			board = new Board(x, o);
			Play();
		}else{
			System.out.println("Thank you for playing.");
		}
	}

	
	



}
