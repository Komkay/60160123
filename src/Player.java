//16/08/2019
public class Player {
	private String name;
	private int win;
	private int draw;
	private int lose;
	
	public Player(String name) {
		this.name = name;
		win = 0;
		lose = 0;
		draw = 0;
	}

	public String getName() {
		return name;
	}
	public void win() {
		win++;
	}
	public void lose() {
		lose++;
	}
	public void draw() {
		draw++;
	}
	public int getWin() {
		return win;
	}
	public int getLose() {
		return lose;
	}
	public int getDraw() {
		return draw;
	}
	public void setWin() {
		win++;
	}

	public void setLose() {
		lose++;
	}

	public void setDraw() {
		draw++;
	}

	
	
}
