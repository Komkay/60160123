import java.util.Scanner;

public class OX_game {
	static Scanner ms = new Scanner(System.in);
	static char[][] board = {

			{ ' ', '1', '2', '3' }, { '1', '-', '-', '-' }, { '2', '-', '-', '-' }, { '3', '-', '-', '-' } };

	static void printwelcome() {
		System.out.println("OX Game");
	}

	static void printboard() {
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board.length; j++) {
				System.out.print(board[i][j] + " ");
			}
			System.out.println();
		}

	}

	static void input() {
		try {
			while (true) {
				System.out.print("Please input row col : ");
				int row = ms.nextInt();
				int cal = ms.nextInt();
				if (board[row][cal] == '-') {
					board[row][cal] = player;
					break;
				} else {
					System.out.println("!!!It's not turn you, please input again :");
					System.out.println("_________________________________________");
				}
			}
		} catch (Exception e) {
			System.out.println("!!!Out of length, please input again :");
			System.out.println("______________________________________");
		}
	}

	static void checkwin() {
		horizontal(); // �ǹ͹
		vertical(); // �ǵ��
		diagonal(); // ���§���
		checkdraw();
	}

	static void checkdraw() {
		if (player == 9)
			AlertBreak = 1;
	}

	static void switchturn() {

		if (player == 'X') {
			player = 'O';
		} else
			player = 'X';
	}

	static void horizontal() {
		if ((board[1][1] == board[1][2] && board[1][1] == board[1][3] && board[1][1] != '-')
				|| (board[2][1] == board[2][2] && board[2][1] == board[2][3] && board[2][1] != '-')
				|| (board[3][1] == board[3][2] && board[1][1] == board[3][3] && board[3][1] != '-')) {
			if (player == 'X') {
				results = 1;
			} else {
				results = 2;
			}
			AlertBreak = 1;
		}

	}

	static void vertical() {
		if ((board[1][1] == board[2][1] && board[1][1] == board[3][1] && board[1][1] != '-')
				|| (board[1][2] == board[2][2] && board[1][2] == board[3][2] && board[1][2] != '-')
				|| (board[1][3] == board[2][3] && board[1][3] == board[3][3] && board[1][3] != '-')) {
			if (player == 'X') {
				results = 1;
			} else {
				results = 2;
			}
			AlertBreak = 1;
		}
	}

	static void diagonal() {
		if ((board[1][1] == board[2][2] && board[1][1] == board[3][3] && board[1][1] != '-')
				|| (board[3][1] == board[2][2] && board[1][3] == board[3][1] && board[3][1] != '-')) {
			if (player == 'X') {
				results = 1;
			} else {
				results = 2;
			}
			AlertBreak = 1;
		}
	}

	static void printwin() {
		if (results == 1) {
			System.out.println("!!!___X Win");
		} else if (results == 2) {
			System.out.println("!!!___O Win");
		} else {
			System.out.println("________Drew");
		}
	}

	static char player = 'X';
	static int row, cal;
	static int results = 0;
	static int AlertBreak = 0;

	public static void main(String[] args) {

		printwelcome();
		while (true) {

			printboard();
			input();
			checkwin();

			if (AlertBreak == 1) {
				break;
			}
			switchturn();
		}
		printboard();
		checkwin();
		printwin();
	}

}
